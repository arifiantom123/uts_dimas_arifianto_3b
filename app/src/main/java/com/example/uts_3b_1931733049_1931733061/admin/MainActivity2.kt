package com.example.uts_3b_1931733049_1931733061.admin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.example.uts_3b_1931733049_1931733061.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.activity_main2.edId
import kotlinx.android.synthetic.main.activity_main2.edName


class MainActivity2 : AppCompatActivity(), View.OnClickListener {
    var fbAuth = FirebaseAuth.getInstance()
    val COLL = "paket"
    val F_ID = "id"
    val F_NAME = "name"
    val F_ADDR = "address"
    val F_PHONE = "phone"

    val F_DETAIL = "detail"
    var docId = ""
    var id=""
    lateinit var db : FirebaseFirestore
    lateinit var alStudent :ArrayList<HashMap<String,Any>>
    lateinit var adapter: SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        alStudent = ArrayList()
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)


        lsData.setOnItemClickListener(itemClick)

    }

    override fun onStart() {
        super.onStart()
        db= FirebaseFirestore.getInstance()
        db.collection(COLL).addSnapshotListener { querySnapshot, e -> if(e != null) Log.d("fireStore",e.message)
            showData() }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                val hm = HashMap<String, Any>()
                hm.set(F_ID,edId.text.toString())
                hm.set(F_NAME,edName.text.toString())
                hm.set(F_ADDR,edAddress.text.toString())
                hm.set(F_PHONE,edPhone.text.toString())
                hm.set(F_DETAIL,edDetailPaket.text.toString())
                db.collection(COLL).document(edId.text.toString()).set(hm).
                addOnSuccessListener {
                    Toast.makeText (this,"Data Successfuly added", Toast.LENGTH_SHORT)
                        .show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this,
                    "Data Unsuccessfully added : ${e.message}",Toast.LENGTH_SHORT)
                    .show()
                }
                }
            R.id.btnUpdate ->{
                val hm = HashMap<String, Any>()
                hm.set(F_ID,docId)
                hm.set(F_NAME,edName.text.toString())
                hm.set(F_ADDR,edAddress.text.toString())
                hm.set(F_PHONE,edPhone.text.toString())
                hm.set(F_DETAIL,edDetailPaket.text.toString())
                db.collection(COLL).document(docId).update(hm)
                    .addOnSuccessListener { Toast.makeText (this,"Data Successfuly updated", Toast.LENGTH_SHORT)
                        .show() }
                    .addOnFailureListener { e ->
                        Toast.makeText(this,
                            "Data Unsuccessfully updated : ${e.message}",Toast.LENGTH_SHORT)
                            .show() }

            }
            R.id.btnDelete ->{
                db.collection(COLL).whereEqualTo(F_ID, docId).get().addOnSuccessListener { results->
                    for (doc in results){
                        db.collection(COLL).document(doc.id).delete()
                            .addOnSuccessListener { Toast.makeText (this,"Data Successfuly deleted", Toast.LENGTH_SHORT)
                                .show() }
                            .addOnFailureListener { e ->
                                Toast.makeText(this,
                                    "Data Unsuccessfully deleted  ${e.message}",Toast.LENGTH_SHORT)
                                    .show() }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this,
                        "Cant get data's references  ${e.message}",Toast.LENGTH_SHORT)
                        .show() }
            }
        }
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alStudent.get(position)
        docId = hm.get(F_ID).toString()
        edId.setText(docId)
        edName.setText(hm.get(F_NAME).toString())
        edAddress.setText(hm.get(F_ADDR).toString())
        edPhone.setText(hm.get(F_PHONE).toString())

        edDetailPaket.setText(hm.get(F_DETAIL).toString())

    }

    fun showData(){
        db.collection(COLL).get().addOnSuccessListener { result ->
            alStudent.clear()
            for(doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAME, doc.get(F_NAME).toString())
                hm.set(F_ADDR, doc.get(F_ADDR).toString())
                hm.set(F_PHONE, doc.get(F_PHONE).toString())
                hm.set(F_DETAIL, doc.get(F_DETAIL).toString())
                alStudent.add(hm)
            }
            adapter = SimpleAdapter(this, alStudent, R.layout.row_data2,
                arrayOf(F_ID,F_NAME,F_ADDR,F_PHONE,F_DETAIL),
                intArrayOf(R.id.txId, R.id.txName, R.id.txAddress, R.id.txPhone, R.id.txDetailPaket))
                        lsData.adapter = adapter
        }
    }


}