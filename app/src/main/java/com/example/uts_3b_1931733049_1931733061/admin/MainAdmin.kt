package com.example.uts_3b_1931733049_1931733061.admin

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.uts_3b_1931733049_1931733061.MainActivity
import com.example.uts_3b_1931733049_1931733061.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.dasbor_admin.*

class MainAdmin : AppCompatActivity(), View.OnClickListener {
    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dasbor_admin)
        btnStore.setOnClickListener(this)
        btnStorage.setOnClickListener(this)

        btnLogoff.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnStorage ->{

                var intent = Intent(this, MainStorage::class.java)
                startActivity(intent)
            }
            R.id.btnLogoff -> {
            fbAuth.signOut()
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

            R.id.btnStore -> {
                var intent = Intent(this, MainActivity2::class.java)
                startActivity(intent)

            }
        }
    }
}