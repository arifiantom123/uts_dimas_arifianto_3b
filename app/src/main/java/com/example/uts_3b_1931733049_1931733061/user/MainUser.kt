package com.example.uts_3b_1931733049_1931733061.user

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.uts_1931733049_1931733061.user.MainStorageUser
import com.example.uts_3b_1931733049_1931733061.MainActivity
import com.example.uts_3b_1931733049_1931733061.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.dasbor_user.*

class MainUser : AppCompatActivity(), View.OnClickListener {
    var fbAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dasbor_user)
        btnStoreUser.setOnClickListener(this)
        btnStorageUser.setOnClickListener(this)

        btnLogoffUser.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnStorageUser ->{

                var intent = Intent(this, MainStorageUser::class.java)
                startActivity(intent)
            } R.id.btnLogoffUser -> {
            fbAuth.signOut()
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

            R.id.btnStoreUser -> {
                var intent = Intent(this, MainActivity2User::class.java)
                startActivity(intent)

            }
        }
    }
}