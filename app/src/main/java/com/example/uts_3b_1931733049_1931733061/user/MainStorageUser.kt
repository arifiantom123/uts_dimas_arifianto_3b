package com.example.uts_1931733049_1931733061.user

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.uts_3b_1931733049_1931733061.R
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.daftar_portofolio.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MainStorageUser : AppCompatActivity(){

    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter : CustomAdapter3User
    lateinit var uri : Uri
    val F_NAME = "file_name"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val RC_OK = 100
    var fileType =""
    var fileName =""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.daftar_portofolio)

        //btnUpPdf.setOnClickListener(this)
       // btnUpImg.setOnClickListener(this)
        //btnUpVid.setOnClickListener(this)
       // btnUpWord.setOnClickListener(this)
        //btnUpload.setOnClickListener(this)

        alFile = ArrayList()
        uri=Uri.EMPTY
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("files")
        db.addSnapshotListener { querysnapshot, firebaseFirestoreException ->
            if(firebaseFirestoreException!=null){
                Log.e("firestore :", firebaseFirestoreException.message)
            }
            showData()
        }
    }

    fun showData(){
        db.get().addOnSuccessListener {result ->
            alFile.clear()
            for(doc in result){
                val hm = HashMap<String ,Any>()
                hm.put(F_NAME, doc.get(F_NAME).toString())
                hm.put(F_TYPE, doc.get(F_TYPE).toString())
                hm.put(F_URL, doc.get(F_URL).toString())
                alFile.add(hm)
            }
            adapter = CustomAdapter3User(this,alFile)
            lsPortofolio.adapter = adapter
        }
    }




    }
